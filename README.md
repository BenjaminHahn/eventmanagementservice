# Event Management Service (EMS)#

## Übersicht ##

* Es wird zwischen verschiedenen Nutzergruppen unterschieden.
* Nutzern der App ist es möglich Events zu Locations hinzuzufügen.
* Andere Nutzer können zu diesen Events Reservierungen hinzufügen.


* Eine kurze Einleitung finden Sie in meinem [YouTube-Kanal]().
* MainTabHolder ist die Hauptklasse


* Die Anwendung wurde mithilfe einer Mock-Implementierung einer Datenbank realisiert.
* Die Datenbank finden Sie in diesem [Repository](https://bitbucket.org/BenGallus/ems_database/overview).