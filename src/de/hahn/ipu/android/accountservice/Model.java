package de.hahn.ipu.android.accountservice;

import android.os.AsyncTask;
import android.util.Log;
import de.hahn.hhn.ipu.mock.HahnEventManagement;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.Location;
import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.InvalidSessionException;
import de.hhn.seb.ipu.app.exceptions.NoReservationPossibleException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.UserDirectory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * provides methods for the communication to the mock implementation
 * Created by post_000 on 03.01.14.
 */
public final class Model {
    private static final String TAG = Model.class.getName();
    private static Model self;
    private final Event mEmptyEvent = new Event("empty", "empty", new Location("empty", "empty", null), new Date(), new Date());
    int countEvents;
    ArrayList<Booking> allBookings;
    private HahnEventManagement emgmt = new HahnEventManagement();
    private Session session;
    private ArrayList<Event> mEvents;
    private ArrayList<Event> mEventsConsideringRole;
    private ArrayList<Booking> mBookings;
    private ArrayList<Location> mLocations;
    private HashMap<String, Event> mEventViaId = new HashMap<>();
    private HashMap<String, ArrayList> mBookingsForEvent = new HashMap<>();
    private User mUser = new User("anonymous", "secret", "firstname", "lastname", UserDirectory.ROLES_SIMPLE);

    private Model() {
        tearDown();
    }

    public static Model getReference() {
        if (self == null) {
            Log.d(TAG, "creating new model");
            self = new Model();
            return self;
        }
        Log.d(TAG, "returning existing model");
        return self;
    }

    /**
     * logs the user into the service
     *
     * @param username
     * @param password
     * @return success or not
     */
    public boolean login(String username, String password) {
        try {
            session = emgmt.login(username, password);
            mUser = emgmt.getUser(session);
        } catch (InvalidParameterException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * checks the user out of the service
     * deletes all lokal data
     *
     * @return success or not
     */
    public boolean logout() {
        try {
            emgmt.logout(session);
            tearDown();
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public User getUser() {
        return mUser;
    }

    public ArrayList<User> getUsers() {
        HashMap<String, User> users = UserDirectory.getReference().getUsers();
        return new ArrayList(users.values());
    }

    public boolean registerUser(User user) {
        // if username already taken register not possible
        for (User existingUser : getUsers()) {
            if (existingUser.getUsername().equals(user.getUsername())) {
                return false;
            }
        }
        UserDirectory.getReference().addUser(user);
        return true;
    }

    public boolean changeUser(User changedUser) {
        String nameBefore = mUser.getUsername();

        // check if username has change and check if changed
        if(!nameBefore.equals(changedUser.getUsername())) {
            for (User existingUser : getUsers()) {
                if (existingUser.getUsername().equals(changedUser.getUsername())) {
                    return false;
                }
            }
        }
        mUser.setUsername(changedUser.getUsername());
        mUser.setPassword(changedUser.getPassword());
        mUser.setFirstname(changedUser.getFirstname());
        mUser.setLastname(changedUser.getLastname());
        for (Booking booking : mBookings) {
            if (booking.getUserName().equals(nameBefore)) {
                booking.setUserName(mUser.getUsername());
            }
        }
        return true;
    }

    /**
     * adds a event to the service
     *
     * @param event to add
     * @return sucess
     */
    public boolean addEvent(Event event) {
        try {
            emgmt.addEvent(session, event);
            System.out.println(event.isAllDayEvent());
        } catch (InvalidParameterException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            return false;
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * get events from the mock implementation
     *
     * @return collection of events
     */
    public ArrayList<Event> getEvents(boolean fromService) {
        Log.d(TAG, "getEvents");
        if (fromService || mEvents == null) {
            mEvents = new ArrayList<>(emgmt.getEvents());

            // store event and booking additional data as soon as possible for quicker access
            AsyncTask<ArrayList<Event>, Void, Void> eventViaIdTask = new AsyncTask<ArrayList<Event>, Void, Void>() {

                class FetchEventViaId implements Runnable {
                    Event event;

                    public FetchEventViaId(Event event) {
                        this.event = event;
                    }

                    @Override
                    public void run() {
                        getEventViaId(event.getId());
                    }
                }

                @Override
                protected Void doInBackground(ArrayList<Event>... events) {
                    mEventViaId.clear();
                    for (Event event : events[0]) {
                        FetchEventViaId run = new FetchEventViaId(event);
                        new Thread(run).start();
                    }
                    Log.d(TAG, "eventsViaId stored");
                    return null;
                }
            };

            AsyncTask<ArrayList<Event>, Void, Void> bookingsEvent = new AsyncTask<ArrayList<Event>, Void, Void>() {

                class FetchBookingForEvent implements Runnable {
                    Event event;

                    public FetchBookingForEvent(Event event) {
                        this.event = event;
                    }

                    @Override
                    public void run() {
                        getBookingsForEvent(event);
                    }
                }

                @Override
                protected Void doInBackground(ArrayList<Event>... events) {
                    mBookingsForEvent.clear();
                    for (Event event : events[0]) {
                        FetchBookingForEvent run = new FetchBookingForEvent(event);
                        new Thread(run).start();
                    }
                    Log.d(TAG, "bookingsForEvent stored");
                    return null;
                }
            };
            eventViaIdTask.execute(mEvents);
            bookingsEvent.execute(mEvents);

            return mEvents;
        } else {
            return mEvents;
        }
    }

    public ArrayList<Event> getEventsConsideringRole(boolean fromService) {
        Log.d(TAG, "getEventsConsideringRole");
        if (fromService || mEventsConsideringRole == null) {
            // return all event or only reservable ones
            if (isUserManager()) {
                mEventsConsideringRole = new ArrayList(emgmt.getEvents());
                return mEventsConsideringRole;
            } else {
                Collection<Event> events = emgmt.getEvents();
                ArrayList<Event> reservableEvents = new ArrayList<>();
                for (Event event : events) {
                    if (event.isReservationPossible()) {
                        reservableEvents.add(event);
                    }
                }
                mEventsConsideringRole = reservableEvents;
                return mEventsConsideringRole;
            }
        } else {
            return mEventsConsideringRole;
        }
    }

    public boolean deleteEvents(List<Event> events) {
        Log.d(TAG, "deleteEvents");
        try {
            for (Event event : events) {
                emgmt.removeEvent(session, event.getId());
            }
        } catch (InvalidParameterException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            return false;
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean addBooking(Booking booking) {
        Log.d(TAG, "addBooking");
        try {
            emgmt.addBooking(session, booking);
        } catch (InvalidParameterException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            return false;
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
            return false;
        } catch (NoReservationPossibleException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<Booking> getBookingsForEvent(Event event) {
        Log.d(TAG, "getBookingsForEvent");
        try {
            String id = event.getId();
            ArrayList<Booking> bookings;

            bookings = mBookingsForEvent.get(id);
            if (bookings != null) {
                return bookings;
            } else {
                bookings = new ArrayList<>(emgmt.getBookingsForEvent(session, event.getId()));
                mBookingsForEvent.put(event.getId(), bookings);
                return bookings;
            }
        } catch (InvalidParameterException e) {
            e.printStackTrace();
        } catch (InvalidSessionException e) {
            e.printStackTrace();
        } catch (NoReservationPossibleException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public boolean deleteBookings(List<Booking> bookings) {
        Log.d(TAG, "deleteBookings");
        try {
            for (Booking booking : bookings) {
                Log.i(TAG, "try to delete booking " + booking.getId());
                emgmt.removeBooking(session, booking.getEventId(), booking.getId());
            }
        } catch (InvalidParameterException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            return false;
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * get all the locations of the service
     *
     * @return array of location
     */
    public ArrayList<Location> getLocations(boolean fromService) {
        Log.d(TAG, "getLocations");
        if (fromService || mLocations == null) {
            mLocations = new ArrayList(emgmt.getLocations());
            return mLocations;
        } else {
            return mLocations;
        }
    }

    public ArrayList<Booking> getAllBookings(boolean fromService) {
        Log.d(TAG, "getAllBookings");
        // for the actuall bookings the events must be up to date to avoid an exception if a event was deleted
        ArrayList<Event> allEvents = getEvents(false);
        countEvents = allEvents.size();
        allBookings = new ArrayList<>();

        if (fromService || mBookings == null) {
            for (Event event : allEvents) {

                class FetchAllBookings implements Runnable {
                    Event event;

                    public FetchAllBookings(Event event) {
                        this.event = event;
                    }

                    @Override
                    public void run() {
                        try {
                            Log.d(TAG, "started " + countEvents);
                            allBookings.addAll(emgmt.getBookingsForEvent(session, event.getId()));
                            countEvents--;
                            Log.d(TAG, "finished " + countEvents);
                        } catch (InvalidParameterException e) {
                            countEvents--;
                            e.printStackTrace();
                        } catch (InvalidSessionException e) {
                            e.printStackTrace();
                        } catch (NoReservationPossibleException e) {
                            e.printStackTrace();
                        }
                    }
                }
                FetchAllBookings run = new FetchAllBookings(event);
                new Thread(run).start();
            }
            // hold thread which returns the booking until all fetching threads are finished
            while (countEvents != 0) {
                // TODO busy waiting is bad
            }
            mBookings = allBookings;
            return mBookings;
        } else {
            return mBookings;
        }
    }

    public Event getEventViaId(String id) {
        Log.d(TAG, "getEventViaId");
        Event storedEvent = mEventViaId.get(id);
        if (storedEvent != null) {
            return storedEvent;
        } else {
            for (Event newEvent : mEvents) {
                if (newEvent.getId().equals(id)) {
                    mEventViaId.put(id, newEvent);
                    return newEvent;
                }
            }
        }
        return mEmptyEvent;
    }

    /**
     * could be done also locally therefore no delay
     *
     * @return true when user is manager
     */
    public boolean isUserManager() {
        Log.d(TAG, "isUserManager");
        return mUser.getRoles().contains(UserDirectory.ROLE_EVENTMANAGER);
    }

    public boolean isLoggedIn() {
        return session != null;
    }

    public void tearDown() {
        Log.d(TAG, "tearDown");
        session = null;
        mUser = new User("anonymous", "secret", "firstname", "lastname", UserDirectory.ROLES_SIMPLE);
        mEvents = null;
        mEventsConsideringRole = null;
        mBookings = null;
        mLocations = null;
        mEventViaId = new HashMap<>();
        mBookingsForEvent = new HashMap<>();
    }
}
