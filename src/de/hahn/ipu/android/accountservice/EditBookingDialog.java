package de.hahn.ipu.android.accountservice;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.adapter.SingleLineEventsAdapter;
import de.hahn.ipu.android.accountservice.util.Helpers;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by post_000 on 05.01.14.
 */
public class EditBookingDialog extends DialogFragment {
    private static final String TAG = EditBookingDialog.class.getName();
    NumberPicker mNoPicker;
    Spinner mEventsSpinner;
    EditText mComment;
    Event mEvent;
    boolean mEditBooking = false;
    private Booking mBookingToEdit;
    private Model mModel = Model.getReference();
    private Fragment mTargetFragment;
    private int mTargetRequestCode;

    public EditBookingDialog() {
    }

    public EditBookingDialog(Event event) {
        mEvent = event;
    }

    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_create_booking, null);

        // fetch Booking
        mBookingToEdit = Helpers.getBookingFromHolder();
        mEditBooking = mBookingToEdit != null;
        mTargetFragment = getTargetFragment();
        mTargetRequestCode = getTargetRequestCode();

        // create alertdialog containing save and cancel button
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (mEditBooking) {
            builder.setTitle("Buchung bearbeiten");
        } else {
            builder.setTitle("Neue Buchung");
        }
        builder.setPositiveButton("Speichern", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveBooking();
            }
        });
        builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        // set layout
        mNoPicker = (NumberPicker) view.findViewById(R.id.create_booking_number_participans);
        mEventsSpinner = (Spinner) view.findViewById(R.id.create_booking_spinner_event);
        mComment = (EditText) view.findViewById(R.id.create_booking_comment);

        // customize view when editing a booking
        if (mEditBooking) {
            Event event = mModel.getEventViaId(mBookingToEdit.getEventId());
            mEventsSpinner.setVisibility(Spinner.GONE);

            int freeParticipants = event.getMaxNoParticipants() - event.getNoActualParticipants();
            int participantsBooking = mBookingToEdit.getNoParticipants();
            // set picker
            mNoPicker.setMaxValue(freeParticipants + participantsBooking > 99 ? 99 : freeParticipants + participantsBooking);
            mNoPicker.setMinValue(1);
            mNoPicker.setValue(participantsBooking);

            // retrieve set comment when existing
            String comment = mBookingToEdit.getAttributes().get("comment");
            mComment.setText(comment != null ? comment : null);

        } else {
            // customize view when creating a new booking
            mEventsSpinner.setAdapter(new SingleLineEventsAdapter(getActivity(), new ArrayList(Model.getReference().getEventsConsideringRole(false)), true));
            mEventsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // set min/max value of numpicker by selected event
                    Log.i(TAG, "item selected");
                    Event event = (Event) mEventsSpinner.getSelectedItem();
                    int freeParticipants = event.getMaxNoParticipants() - event.getNoActualParticipants();
                    mNoPicker.setMaxValue(freeParticipants > 99 ? 99 : freeParticipants);
                    mNoPicker.setMinValue(1);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if (mEvent != null) {
            SingleLineEventsAdapter adapter = (SingleLineEventsAdapter) mEventsSpinner.getAdapter();
            mEventsSpinner.setSelection(adapter.getIndexOfEvent(mEvent));
        }

        builder.setView(view);
        return builder.create();
    }

    public void saveBooking() {
        HashMap<String, String> attributes = new HashMap<String, String>();

        if (mEditBooking) {
            mBookingToEdit.setNoParticipants(mNoPicker.getValue());
            attributes.put("comment", mComment.getText().toString());
            mBookingToEdit.setAttributes(attributes);

            // send booking to Fragment
            mTargetFragment.onActivityResult(mTargetRequestCode, 0, null);
            Toast.makeText(getActivity(), "Buchung wurde verändert", Toast.LENGTH_SHORT);
        } else {
            Event event = (Event) mEventsSpinner.getSelectedItem();
            int noParticipants = mNoPicker.getValue();
            Booking booking = new Booking(event.getId(), noParticipants);

            // add attributes
            attributes.put("comment", mComment.getText().toString());
            booking.setAttributes(attributes);

            // send result to Fragment
            new AddBooking().execute(booking);
            dismiss();
        }
    }

    class AddBooking extends AsyncTask<Booking, Void, Void> {

        @Override
        protected void onPreExecute() {
            MainTabHolder.showUpdatespinner();
        }

        @Override
        protected Void doInBackground(Booking... booking) {
            Model.getReference().addBooking(booking[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(mTargetFragment.getActivity(), "Buchung wurde hinzugefügt", Toast.LENGTH_SHORT).show();
            mTargetFragment.onActivityResult(mTargetRequestCode, 0, null);
            MainTabHolder.hideUpdateSpinner();
        }
    }
}