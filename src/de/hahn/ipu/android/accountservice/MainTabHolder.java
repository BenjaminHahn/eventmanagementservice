package de.hahn.ipu.android.accountservice;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.adapter.SingleLineEventsAdapter;
import de.hahn.ipu.android.accountservice.util.DefeatableViewPager;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by post_000 on 03.01.14.
 */
public class MainTabHolder extends FragmentActivity {
    private static final String TAG = MainTabHolder.class.getName();
    private static final String EVENTS = "Event";
    private static final String BOOKINGS = "Buchungen";
    private static final String LOCATIONS = "Locations";
    public static final int CREATE_EVENT = 1;
    public static final int EDIT_EVENT = 2;
    private static MenuItem mMenuSync;
    private static int threadsRunning = 0;
    private static Handler UIHandler = new Handler(Looper.getMainLooper());
    private static DefeatableViewPager mDefeatableViewPager;
    private Model mModel = Model.getReference();
    private ActionBar mActionbar;
    private ShowBookingsFragment mBookingsFragment;
    private ShowLocationsFragment mLocationsFragment;
    private ShowEventsFragment mEventsFragment;
    private static Fragment lastSelectedFragment;

    /**
     * status of running threads to service
     *
     * @return status sync in progress
     */
    public static boolean isThreadRunning() {
        return threadsRunning > 0;
    }

    /**
     * show spinning progress instead of sync actionbar item
     * indicates a data transfer to service
     */
    public static void showUpdatespinner() {
        threadsRunning++;
        Log.d(TAG, "showUpdatespinner running: " + threadsRunning);
        if (threadsRunning != 0) {
            UIHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mMenuSync != null) {
                        mMenuSync.setActionView(R.layout.item_progress);
                        mMenuSync.expandActionView();
                    }
                }
            });
        }
    }

    /**
     * shows sync icon instead of spinning progress
     * indcates that there's no data transfer to service
     */
    public static void hideUpdateSpinner() {
        threadsRunning--;
        Log.d(TAG, "hideUpdateSpinner running: " + threadsRunning);
        if (threadsRunning == 0) {
            UIHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mMenuSync != null) {
                        mMenuSync.collapseActionView();
                        mMenuSync.setActionView(null);
                    }
                }
            });
        }
    }

    public static void setPageChangeable(boolean changeable) {
        mDefeatableViewPager.setSwipeable(changeable);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        mMenuSync = menu.findItem(R.id.fragment_holder_menu_sync);
        mMenuSync.collapseActionView();
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu");
        // customize actionbar when user is loggen in or not
        if (mModel.isLoggedIn()) {
            menu.findItem(R.id.fragment_holder_menu_login_logout).setIcon(R.drawable.ic_arrow_door_out);
            menu.findItem(R.id.fragment_holder_menu_profile).setVisible(true);
        } else {
            menu.findItem(R.id.fragment_holder_menu_login_logout).setIcon(R.drawable.ic_arrow_door_in);
            menu.findItem(R.id.fragment_holder_menu_profile).setVisible(false);
        }

        // change items on actionbar according to user role
        if (mModel.isUserManager()) {
            switch (mActionbar.getSelectedTab().getText().toString()) {
                case LOCATIONS:
                    menu.findItem(R.id.fragment_holder_menu_add).setVisible(false);
                    break;
            }
        } else {
            switch (mActionbar.getSelectedTab().getText().toString()) {
                case EVENTS:
                    menu.findItem(R.id.fragment_holder_menu_add).setVisible(false);
                    break;
                case LOCATIONS:
                    menu.findItem(R.id.fragment_holder_menu_add).setVisible(false);
                    break;
            }
        }

        // keep sync progress spinning when one or more threads to the service are running
        if (threadsRunning > 0) {
            mMenuSync.setActionView(R.layout.item_progress);
            mMenuSync.expandActionView();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.fragment_holder_menu_add:
                // context sensitive add button, add event or booking
                switch (getActionBar().getSelectedTab().getText().toString()) {
                    case EVENTS:
                        startActivityForResult(new Intent(this, EditEvent.class), CREATE_EVENT);
                        break;
                    case BOOKINGS:
                        DialogFragment fragment = new EditBookingDialog();
                        fragment.show(getSupportFragmentManager(), "add Booking");
                        fragment.setTargetFragment(mBookingsFragment, ShowBookingsFragment.CREATE_BOOKING);
                        break;
                }
                break;
            case R.id.fragment_holder_menu_sync:
                // sync item to update the lokal data from the service
                switch (getActionBar().getSelectedTab().getText().toString()) {
                    case EVENTS:
                        mEventsFragment.populateView();
                        break;
                    case BOOKINGS:
                        mBookingsFragment.populateView();
                        break;
                    case LOCATIONS:
                        mLocationsFragment.populateView();
                        break;
                }
                break;
            case R.id.fragment_holder_menu_login_logout:
                if (mModel.isLoggedIn()) {
                    new LogoutTask(this).execute((Void) null);
                } else {
                    startActivity(new Intent(this, Login.class));
                }
                break;
            case R.id.fragment_holder_menu_profile:
                startActivity(new Intent(this, EditUser.class));
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_fragment_holder);
        setTitle("Event Management Service");

        // action bar tabs
        mActionbar = getActionBar();
        mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        final ActionBar.Tab eventsTab = mActionbar.newTab().setText(EVENTS);
        final ActionBar.Tab locationsTab = mActionbar.newTab().setText(LOCATIONS);

        mEventsFragment = new ShowEventsFragment();
        mLocationsFragment = new ShowLocationsFragment();

        Fragment[] fragments = new Fragment[]{mEventsFragment, mLocationsFragment};
        mDefeatableViewPager = (DefeatableViewPager) findViewById(R.id.main_actiontab_viewpager);
        mDefeatableViewPager.setOffscreenPageLimit(2);
        mDefeatableViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), Arrays.asList(fragments)));
        mDefeatableViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getActionBar().setSelectedNavigationItem(position);
            }
        });

        eventsTab.setTabListener(new MyTabListener(mEventsFragment));
        locationsTab.setTabListener(new MyTabListener(mLocationsFragment));

        mActionbar.addTab(eventsTab);
        mActionbar.addTab(locationsTab);

        if (mModel.isLoggedIn()) {
            onLogin();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // update event list when event was added or changed
        Log.d(TAG, "onActivityResult" + "requestCode/resultcode " + requestCode + " / " + resultCode);
        // add event to view when new one is created without sync to service
        // when editing event update list
        switch (resultCode) {
            case CREATE_EVENT:
                Event event = (Event) data.getSerializableExtra("event");
                mEventsFragment.addEventToList(event);
                break;
            case EDIT_EVENT:
                mEventsFragment.refreshList();
                break;
        }
    }

    /**
     * add information on login
     */
    public void onLogin() {
        final ActionBar.Tab bookingsTab = mActionbar.newTab().setText(BOOKINGS);

        mBookingsFragment = new ShowBookingsFragment();
        bookingsTab.setTabListener(new MyTabListener(mBookingsFragment));
        mActionbar.addTab(bookingsTab, 1);
        Fragment[] fragments = new Fragment[]{mEventsFragment, mBookingsFragment, mLocationsFragment};
        mDefeatableViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), Arrays.asList(fragments)));
    }

    /**
     * select second tab and set the spinner to specific event
     *
     * @param event to be set
     */
    public void selectEventOnBookingTab(Event event) {
        Log.d(TAG, "selectEventOnBookingTab");
        getActionBar().setSelectedNavigationItem(1);
        Spinner spinner = (Spinner) findViewById(R.id.show_bookings_spinner_events);
        SingleLineEventsAdapter adapter = (SingleLineEventsAdapter) spinner.getAdapter();
        spinner.setSelection(adapter.getIndexOfEvent(event));
    }

    /**
     * is needed to send the right target in the eventsfragment
     * there must e a better way but time is short
     *
     * @return the actual used bookingFragment
     */
    public ShowBookingsFragment getBookingFragment() {
        return mBookingsFragment;
    }

    /**
     * set second tab an show a list of booking from a list of events
     *
     * @param events which bookings should be shown
     */
    public void showBookingsOfMultipleEvents(ArrayList<Event> events) {
        Log.d(TAG, "showBookingsOfMultipleEvents");
        getActionBar().setSelectedNavigationItem(1);
        AsyncTask<ArrayList<Event>, Void, ArrayList<Booking>> multiBookingsTask = new AsyncTask<ArrayList<Event>, Void, ArrayList<Booking>>() {

            @Override
            protected ArrayList<Booking> doInBackground(ArrayList<Event>... events) {
                ArrayList<Booking> bookings = new ArrayList<>();
                for (Event event : events[0]) {
                    bookings.addAll(mModel.getBookingsForEvent(event));
                }
                return bookings;
            }

            @Override
            protected void onPostExecute(ArrayList<Booking> bookings) {
                mBookingsFragment.setListViewItems(bookings);
            }
        };
        multiBookingsTask.execute(events);
    }

    /**
     * changes the fragments on tab selection
     */
    class MyTabListener implements ActionBar.TabListener {
        private Fragment mFragment;
        private FragmentTransaction mFt;

        public MyTabListener(Fragment fragment) {
            mFragment = fragment;
            mFt = getSupportFragmentManager().beginTransaction();
        }

        @Override
        public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            // deselect actionmode on actual fragment
            if (lastSelectedFragment != null) ((DisableActionMode) lastSelectedFragment).disableActionMode();
            // set actual fragment
            lastSelectedFragment = mFragment;
            // change view
            mDefeatableViewPager.setCurrentItem(tab.getPosition());
            invalidateOptionsMenu();
        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            mFt.remove(mFragment);
        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

        }
    }

    /**
     * adapter for viewpager
     */
    class MyPagerAdapter extends FragmentStatePagerAdapter {
        List<Fragment> mTabs;

        public MyPagerAdapter(FragmentManager manager, List<Fragment> tabs) {
            super(manager);
            mTabs = tabs;
        }

        @Override
        public Fragment getItem(int i) {
            return mTabs.get(i);
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }
    }

    class LogoutTask extends AsyncTask<Void, Void, Void> {
        Context mContext;

        public LogoutTask(Context activity) {
            mContext = activity;
            Toast.makeText(mContext, "Versuche Nutzer abzumelden...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (isThreadRunning()) {

            }
            ActivityManager ma = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            ma.killBackgroundProcesses(getPackageName());
            mModel.logout();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(mContext, "Erfolgreich abgemeldet.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(mContext, MainTabHolder.class));
        }
    }
}