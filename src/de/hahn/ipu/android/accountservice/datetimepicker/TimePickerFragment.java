package de.hahn.ipu.android.accountservice.datetimepicker;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import de.hahn.ipu.android.accountservice.util.Helpers;

import java.util.Calendar;

/**
 * Created by post_000 on 04.01.14.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    TextView mLabelToSet;

    public TimePickerFragment(View labelToSet) {
        mLabelToSet = (TextView) labelToSet;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        c.setTime(Helpers.parseStringToDateTime(mLabelToSet.getText().toString()));
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String h = hourOfDay < 10 ? "0" + hourOfDay : hourOfDay + "";
        String m = minute < 10 ? "0" + minute : minute + "";
        mLabelToSet.setText(h + ":" + m);
    }
}