package de.hahn.ipu.android.accountservice.datetimepicker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.util.Helpers;

import java.util.Calendar;

/**
 * Created by post_000 on 04.01.14.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    TextView mLabelToSet;

    public DatePickerFragment(View labelToSet) {
        mLabelToSet = (TextView) labelToSet;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        c.setTime(Helpers.parseStringToDateDate(mLabelToSet.getText().toString()));
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        String y = year + "";
        month += 1;
        String m = month < 10 ? "0" + month : month + "";
        String d = day < 10 ? "0" + day : day + "";
        mLabelToSet.setText(d + "." + m + "." + y);
    }

}