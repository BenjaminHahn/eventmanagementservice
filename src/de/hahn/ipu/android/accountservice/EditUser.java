package de.hahn.ipu.android.accountservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.util.ClearErrorWatcher;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.UserDirectory;

/**
 * Created by post_000 on 23.01.14.
 */
public class EditUser extends Activity {
    boolean mCancel = false;
    EditText mEdtUsername;
    EditText mEdtPassword;
    EditText mEdtFirstname;
    EditText mEdtLastname;
    Model mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mModel = Model.getReference();

        mEdtUsername = (EditText) findViewById(R.id.user_profile_username);
        mEdtPassword = (EditText) findViewById(R.id.user_profile_password);
        mEdtFirstname = (EditText) findViewById(R.id.user_profile_firstname);
        mEdtLastname = (EditText) findViewById(R.id.user_profile_lastname);

        // when user is logged in get and his profile in form
        if (mModel.isLoggedIn()) {
            setData();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_done_cancel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                saveUser();
                return true;
            case R.id.cancel:
                finish();
                return true;
            default:
                return false;
        }
    }

    /**
     * set data from logged in user into form
     */
    public void setData() {
        User user = mModel.getUser();
        mEdtUsername.setText(user.getUsername());
        mEdtPassword.setText(user.getPassword());
        mEdtFirstname.setText(user.getFirstname());
        mEdtLastname.setText(user.getLastname());
    }

    /**
     * get data from form and save information
     *
     * @return the user object containing information from form
     */
    public User saveData() {

        String username = mEdtUsername.getText().toString();
        mEdtUsername.addTextChangedListener(new ClearErrorWatcher(mEdtUsername));
        if (username.isEmpty()) {
            mEdtUsername.setError("Feld wird benötigt");
            mCancel = true;
            return null;
        }

        String password = mEdtPassword.getText().toString();
        mEdtPassword.addTextChangedListener(new ClearErrorWatcher(mEdtPassword));
        if (password.isEmpty()) {
            mEdtPassword.setError("Feld wird benötigt");
            mCancel = true;
            return null;
        }

        String firstname = mEdtFirstname.getText().toString();
        mEdtFirstname.addTextChangedListener(new ClearErrorWatcher(mEdtFirstname));
        if (firstname.isEmpty()) {
            mEdtFirstname.setError("Feld wird benötigt");
            mCancel = true;
            return null;
        }

        String lastname = mEdtLastname.getText().toString();
        mEdtLastname.addTextChangedListener(new ClearErrorWatcher(mEdtLastname));
        if (lastname.isEmpty()) {
            mEdtLastname.setError("Feld wird benötigt");
            mCancel = true;
            return null;
        }
        return new User(username, password, firstname, lastname, UserDirectory.ROLES_SIMPLE);
    }

    /**
     * save the user into the user directory
     */
    public void saveUser() {
        User user = saveData();
        if (mCancel || user == null) {
            Toast.makeText(this, "Nutzer konnte nicht erstellt werden", Toast.LENGTH_SHORT).show();
            mCancel = false;
        } else {
            // when a user is logged in change user
            if (mModel.isLoggedIn()) {
                if (mModel.changeUser(user)) {
                    startActivity(new Intent(this, MainTabHolder.class));
                } else {
                    mEdtUsername.setError("Nutzername bereits vergeben");
                    mEdtUsername.requestFocus();
                }
            } else {
                // register user and redirect to login
                if (mModel.registerUser(user)) {
                    Intent intent = new Intent(this, Login.class);
                    intent.putExtra("username", user.getUsername());
                    intent.putExtra("password", user.getPassword());
                    startActivity(intent);
                } else {
                    mEdtUsername.setError("Nutzername bereits vergeben");
                    mEdtUsername.requestFocus();
                }
            }
        }
    }
}
