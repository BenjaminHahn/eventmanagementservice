package de.hahn.ipu.android.accountservice.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.R;
import de.hhn.seb.ipu.app.eventmgmt.Event;

import java.util.List;

/**
 * Created by post_000 on 05.01.14.
 */
public class SingleLineEventsAdapter extends BaseAdapter {
    private static final String TAG = SingleLineEventsAdapter.class.getName();
    List mEvents;
    boolean mLongLine = false;
    LayoutInflater mInflater;

    public SingleLineEventsAdapter(Context context, List events) {
        mEvents = events;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public SingleLineEventsAdapter(Context context, List events, boolean longLine) {
        mLongLine = longLine;
        mEvents = events;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mEvents.size();
    }

    @Override
    public Object getItem(int position) {
        return mEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * return the index of a given event
     *
     * @param event
     * @return item position
     */
    public int getIndexOfEvent(Event event) {
        for (int i = 0; i < mEvents.size(); i++) {
            if (event.equals(mEvents.get(i))) {
                Log.i(TAG, "index of " + event.getTitle() + " = " + i);
                return i;
            }
        }
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listitem_single_title, null);

        Event event = (Event) mEvents.get(position);
        TextView title = (TextView) view.findViewById(R.id.listitem_single_title_title);
        title.setText(event.getTitle());

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listitem_single_title, null);

        Event event = (Event) mEvents.get(position);
        TextView titleView = (TextView) view.findViewById(R.id.listitem_single_title_title);
        String title = event.getTitle();
        String shorten;
        if (!mLongLine) {
            shorten = title.length() > 10 ? title.substring(0, 10) + "..." : title;
        } else {
            shorten = title;
        }
        titleView.setText(shorten);

        return view;
    }
}
