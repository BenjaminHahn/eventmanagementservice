package de.hahn.ipu.android.accountservice.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.R;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;

import java.util.List;

/**
 * Created by post_000 on 05.01.14.
 */
public class SingleLineUsersAdapter extends BaseAdapter {
    List mUsers;
    LayoutInflater mInflater;
    private static final String TAG = SingleLineUsersAdapter.class.getName();

    public SingleLineUsersAdapter(Context context, List users) {
        mUsers = users;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * return the index of a given event
     *
     * @param event
     * @return item position
     */
    public int getIndexOfEvent(Event event) {
        for (int i = 0; i < mUsers.size(); i++) {
            if (event.equals(mUsers.get(i))) {
                Log.i(TAG, "index of " + event.getTitle() + " = " + i);
                return i;
            }
        }
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listitem_single_title, null);

        User user = (User) mUsers.get(position);
        TextView titleView = (TextView) view.findViewById(R.id.listitem_single_title_title);
        titleView.setText(user.getUsername());

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listitem_single_title, null);

        User user = (User) mUsers.get(position);
        TextView titleView = (TextView) view.findViewById(R.id.listitem_single_title_title);
        String title = user.getUsername();
        String shorten = title.length() > 10 ? title.substring(0, 10) + "..." : title;
        titleView.setText(shorten);

        return view;
    }
}
