package de.hahn.ipu.android.accountservice.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.R;
import de.hhn.seb.ipu.app.eventmgmt.Location;

import java.util.List;

/**
 * Created by post_000 on 05.01.14.
 */
public class SingleLineLocationsAdapter extends BaseAdapter {
    private static final String TAG = SingleLineLocationsAdapter.class.getName();
    List mLocations;
    boolean mLongLine = false;
    LayoutInflater mInflater;

    public SingleLineLocationsAdapter(Context context, List locations) {
        mLocations = locations;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public SingleLineLocationsAdapter(Context context, List locations, boolean longLine) {
        mLongLine = longLine;
        mLocations = locations;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mLocations.size();
    }

    @Override
    public Object getItem(int position) {
        return mLocations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * return the index of a given even

       @para  locatio
     * @retur  ite  positio
     */
    public int getIndexOfLocation(Location location) {
        for (int i = 0; i < mLocations.size(); i++) {
            if (location.equals(mLocations.get(i))) {
                Log.i(TAG, "index of " + location.getTitle() + " = " + i);
                return i;
            }
        }
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listitem_single_title, null);

        Location location = (Location) mLocations.get(position);
        TextView title = (TextView) view.findViewById(R.id.listitem_single_title_title);
        title.setText(location.getTitle());

        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listitem_single_title, null);

        Location location = (Location) mLocations.get(position);
        TextView titleView = (TextView) view.findViewById(R.id.listitem_single_title_title);
        String title = location.getTitle();
        String shorten;
        // shorten title when spinner should be short
        if (!mLongLine) {
            shorten = title.length() > 10 ? title.substring(0, 10) + "..." : title;
        } else {
            shorten = title;
        }
        titleView.setText(shorten);

        return view;
    }
}
