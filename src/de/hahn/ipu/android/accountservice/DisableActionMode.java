package de.hahn.ipu.android.accountservice;

/**
 * Created by post_000 on 25.01.14.
 */
public interface DisableActionMode {
    public void disableActionMode();
}
