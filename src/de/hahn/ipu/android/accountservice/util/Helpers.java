package de.hahn.ipu.android.accountservice.util;

import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by post_000 on 08.01.14.
 */
public class Helpers {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
    private static final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
    private static Event mEvent;
    private static Booking mBooking;

    public static String formatDateToStringDate(Date date) {
        return dateFormatter.format(date);
    }

    public static String formatDateToStringTime(Date time) {
        return timeFormatter.format(time);
    }

    public static Date parseStringToDateDate(String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static Date parseStringToDateTime(String time) {
        try {
            return timeFormatter.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String getDayName(Calendar calendar) {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return "Sonntag";
            case 2:
                return "Montag";
            case 3:
                return "Dienstag";
            case 4:
                return "Mittwoch";
            case 5:
                return "Donnerstag";
            case 6:
                return "Freitag";
            case 7:
                return "Samstag";
            default:
                return "Sanknimmerleinstag";
        }
    }

    /**
     * hold a event for further use
     *
     * @param event which should be hold
     */
    public static void putEventIntoHolder(Event event) {
        mEvent = event;
    }

    /**
     * get Event and destroy it after the first time
     *
     * @return event which was set
     */
    public static Event getEventFromHolder() {
        Event event = mEvent;
        mEvent = null;
        return event;
    }

    public static void putBookingIntoHolder(Booking booking) {
        mBooking = booking;
    }

    public static Booking getBookingFromHolder() {
        Booking booking = mBooking;
        mBooking = null;
        return booking;
    }
}
