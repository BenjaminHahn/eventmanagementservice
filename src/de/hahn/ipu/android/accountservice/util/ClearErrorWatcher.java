package de.hahn.ipu.android.accountservice.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by post_000 on 20.01.14.
 */
public class ClearErrorWatcher implements TextWatcher {
    TextView mView;

    public ClearErrorWatcher(TextView view) {
        mView = view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // do nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mView.setError(null);
    }

    @Override
    public void afterTextChanged(Editable s) {
        // do nothing
    }
}
