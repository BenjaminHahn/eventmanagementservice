package de.hahn.ipu.android.accountservice.util;

import java.util.concurrent.Executor;

/**
 * Created by post_000 on 16.01.14.
 */
public class ParallelExecutor implements Executor {
    public static ParallelExecutor self;

    @Override
    public void execute(Runnable command) {
        new Thread(command).start();
    }

    public static ParallelExecutor getReference() {
        if (self == null) {
            self = new ParallelExecutor();
            return self;
        } else {
            return self;
        }
    }
}
