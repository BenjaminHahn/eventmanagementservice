package de.hahn.ipu.android.accountservice.util;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by post_000 on 24.01.14.
 */
public class DefeatableViewPager extends ViewPager {
    private static boolean mSwipeable = false;

    public DefeatableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSwipeable = true;
    }

    public static void setSwipeable(boolean swipable) {
        mSwipeable = swipable;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mSwipeable) {
            return super.onInterceptTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mSwipeable) {
            return super.onTouchEvent(ev);
        }
        return false;
    }

}
