package de.hahn.ipu.android.accountservice;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.dialog.EventDetailsDialog;
import de.hahn.ipu.android.accountservice.util.Helpers;
import de.hahn.ipu.android.accountservice.util.ParallelExecutor;
import de.hhn.seb.ipu.app.eventmgmt.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by post_000 on 03.01.14.
 */
public class ShowEventsFragment extends Fragment implements DisableActionMode {
    private static final String TAG = ShowEventsFragment.class.getName();
    private Model mModel;
    private ListView mListView;
    private int mSelectedCount;
    private LinearLayout mProgressLayout;
    private MainTabHolder mTabHolder;
    private ActionMode mActionMode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        System.out.println(Integer.MAX_VALUE);
        View view = inflater.inflate(R.layout.fragment_show_events, container, false);

        mTabHolder = (MainTabHolder) getActivity();
        mModel = Model.getReference();

        // configure list
        mProgressLayout = (LinearLayout) view.findViewById(R.id.progress_progress);
        mListView = (ListView) view.findViewById(R.id.show_events_listview_events);
        mListView.setEmptyView(view.findViewById(R.id.listitem_empty_item));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Event event = (Event) mListView.getAdapter().getItem(position);
                DialogFragment dialog = new EventDetailsDialog(event);
                dialog.show(getActivity().getSupportFragmentManager(), "Details");
            }
        });

        if (mModel.isLoggedIn()) {
            mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            mListView.setMultiChoiceModeListener(new EventCABListener());
        }

        populateView();
        return view;
    }

    public void populateView() {
        Log.d(TAG, "populateView");
        AsyncTask<Void, Void, ArrayList<Event>> populateListView = new AsyncTask<Void, Void, ArrayList<Event>>() {

            @Override
            protected void onPreExecute() {
                Log.d(TAG, "in pre");
                mTabHolder.showUpdatespinner();
                mProgressLayout.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<Event> doInBackground(Void... params) {
                Log.d(TAG, "in doBackground");
                return mModel.getEventsConsideringRole(true);
            }

            @Override
            protected void onPostExecute(ArrayList<Event> events) {
                Log.d(TAG, "in post");
                Log.i(TAG, "events fetched populate listview now");
                mListView.setAdapter(new EventsAdapter(getActivity(), events));
                mTabHolder.hideUpdateSpinner();
                mProgressLayout.setVisibility(View.GONE);
            }
        };
        populateListView.executeOnExecutor(ParallelExecutor.getReference(), (Void) null);
    }

    /**
     * iterates through listview and returns first selected item
     */
    private void editCheckedEvent() {
        Log.d(TAG, "editCheckedEvent");
        Adapter adapter = mListView.getAdapter();
        int[] checkedPositions = getCheckedPositions();
        Event event = (Event) adapter.getItem(checkedPositions[0]);
        Log.i(TAG, "edit event: " + event.getTitle());
        Helpers.putEventIntoHolder(event);
        startActivityForResult(new Intent(getActivity(), EditEvent.class), MainTabHolder.EDIT_EVENT);
    }

    /**
     * get all selected event in cab mode and delete those
     */
    private void deleteCheckedEvents() {
        Log.d(TAG, "deleteCheckedEvents");
        Adapter adapter = mListView.getAdapter();
        ArrayList<Event> events = new ArrayList<Event>();
        int[] checkedPositions = getCheckedPositions();
        for (int i : checkedPositions) {
            events.add((Event) adapter.getItem(i));
        }
        new ConfirmDeleteDialog(getActivity(), events).show(getActivity().getFragmentManager(), "deleteEvents");
    }

    /**
     * create a event in google calender for selected event
     */
    private void createCalendarEvent() {
        Log.d(TAG, "createCalendarEvent");
        Adapter adapter = mListView.getAdapter();
        int[] checkedPositions = getCheckedPositions();
        Event event = (Event) adapter.getItem(checkedPositions[0]);

        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("title", event.getBeginsAt());
        intent.putExtra("allDay", event.isAllDayEvent());
        intent.putExtra("endTime", event.getEndsAt());
        intent.putExtra("title", event.getTitle());
        intent.putExtra("eventLocation", event.getLocation().getTitle());
        intent.putExtra("description", event.getDescription());
        startActivity(intent);
    }

    private void createBookingForEvent() {
        Log.d(TAG, "createBookingForEvent");
        Adapter adapter = mListView.getAdapter();
        int[] checkedPositions = getCheckedPositions();
        Event event = (Event) adapter.getItem(checkedPositions[0]);
        Log.i(TAG, "edit event: " + event.getTitle());

        EditBookingDialog dialog = new EditBookingDialog(event);
        ShowBookingsFragment fragment = mTabHolder.getBookingFragment();
        dialog.setTargetFragment(fragment, ShowBookingsFragment.CREATE_BOOKING);
        dialog.show(getActivity().getSupportFragmentManager(), "create booking");
    }

    private void showBookingsSelectedEvents() {
        Log.d(TAG, "showBookingsSelectedEvents");
        Adapter adapter = mListView.getAdapter();
        ArrayList<Event> events = new ArrayList<>();
        int[] checkedPositions = getCheckedPositions();
        for (int i : checkedPositions) {
            events.add((Event) adapter.getItem(i));
        }
        MainTabHolder activity = (MainTabHolder) getActivity();
        if (events.size() == 1) {
            activity.selectEventOnBookingTab(events.get(0));
        } else {
            activity.showBookingsOfMultipleEvents(events);
        }
    }

    /**
     * get positions of selected items
     *
     * @return item position
     */
    private int[] getCheckedPositions() {
        Log.d(TAG, "getCheckedPositions");
        ArrayList<Integer> positions = new ArrayList<>();
        Adapter adapter = mListView.getAdapter();
        SparseBooleanArray checkedItems = mListView.getCheckedItemPositions();
        for (int i = 0; i < adapter.getCount(); i++) {
            if (checkedItems.get(i)) {
                positions.add(i);
            }
        }
        // convert to primitive
        int[] pos = new int[positions.size()];
        for (int j = 0; j < pos.length; j++) {
            pos[j] = positions.get(j);
        }
        return pos;
    }

    /**
     * customize presentation
     */
    class EventsAdapter extends BaseAdapter {
        List<Event> mEvents;
        LayoutInflater mInflater;

        public EventsAdapter(Context context, List<Event> events) {
            mEvents = events;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mEvents.size();
        }

        @Override
        public Object getItem(int position) {
            return mEvents.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void addItem(Event event) {
            mEvents.add(event);
            notifyDataSetChanged();
        }

        public void removeItems(List<Event> events) {
            for (Event event : events) {
                mEvents.remove(event);
                Log.d(TAG, "removed event locally: " + event.getTitle());
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = mInflater.inflate(R.layout.listitem_fragment_show_events, null);
            Event event = mEvents.get(position);
            Log.d(TAG, "creating listview for " + event.getTitle());

            TextView title = (TextView) view.findViewById(R.id.show_events_title);
            TextView location = (TextView) view.findViewById(R.id.show_events_where);
            TextView beginsAtView = (TextView) view.findViewById(R.id.show_events_begin);
            TextView endsAtView = (TextView) view.findViewById(R.id.show_events_end);
            TextView reservableView = (TextView) view.findViewById(R.id.show_events_reservable);
            TextView maxParticipantsView = (TextView) view.findViewById(R.id.show_event_max_participants);
            TextView actualParticipantsView = (TextView) view.findViewById(R.id.show_event_actual_participants);
            TextView freeParticipantsView = (TextView) view.findViewById(R.id.show_events_free_participants);

            TableRow rowEnds = (TableRow) view.findViewById(R.id.show_events_row_ends);
            TableRow rowReservable = (TableRow) view.findViewById(R.id.show_events_row_reservable);
            TableRow rowParticipants = (TableRow) view.findViewById(R.id.show_event_row_participants);
            TableRow rowFreeParticipants = (TableRow) view.findViewById(R.id.show_events_row_free_participants);


            title.setText(event.getTitle());
            location.setText(event.getLocation().getTitle());

            // when event not allday show begin and ending
            Calendar calendar = Calendar.getInstance();
            if (!event.isAllDayEvent()) {
                Date beginsAt = event.getBeginsAt();
                calendar.setTime(beginsAt);
                beginsAtView.setText(Helpers.getDayName(calendar) + ", "
                        + Helpers.formatDateToStringDate(beginsAt) + " "
                        + Helpers.formatDateToStringTime(beginsAt));

                Date endsAt = event.getEndsAt();
                calendar.setTime(beginsAt);
                endsAtView.setText(Helpers.getDayName(calendar) + ", "
                        + Helpers.formatDateToStringDate(endsAt) + " "
                        + Helpers.formatDateToStringTime(endsAt));
            }
            // when event allday just show the date
            else {
                Date beginsAt = event.getBeginsAt();
                calendar.setTime(beginsAt);
                beginsAtView.setText(Helpers.getDayName(calendar) + ", "
                        + Helpers.formatDateToStringDate(beginsAt));

                Date endsAt = event.getEndsAt();
                calendar.setTime(beginsAt);
                endsAtView.setText(Helpers.getDayName(calendar) + ", "
                        + Helpers.formatDateToStringDate(endsAt));
            }

            // customize view for manager
            if (mModel.isUserManager()) {
                rowFreeParticipants.setVisibility(TableRow.GONE);
                reservableView.setText(event.isReservationPossible() ? "Ja" : "Nein");
                actualParticipantsView.setText(event.getNoActualParticipants() + "");

                // check and alter view when indefinite participants
                if (event.getMaxNoParticipants() == Integer.MAX_VALUE) {
                    maxParticipantsView.setText("unbegrenzt");
                } else {
                    maxParticipantsView.setText(event.getMaxNoParticipants() + "");
                }
            }
            // customize view for simple user
            else {
                rowReservable.setVisibility(TableRow.GONE);
                rowParticipants.setVisibility(TableRow.GONE);

                if (event.getMaxNoParticipants() == Integer.MAX_VALUE) {
                    freeParticipantsView.setText("unbegrenzt");
                } else {
                    freeParticipantsView.setText(event.getMaxNoParticipants() - event.getNoActualParticipants() + "");

                }
            }
            return view;
        }
    }

    class DeleteEventsTask extends AsyncTask<List<Event>, Void, Void> {
        int mEventCount;

        @Override
        protected void onPreExecute() {
            MainTabHolder.showUpdatespinner();
        }

        @Override
        protected Void doInBackground(List<Event>... events) {
            mEventCount = events[0].size();
            ((EventsAdapter) mListView.getAdapter()).removeItems(events[0]);
            mModel.deleteEvents(events[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (mEventCount > 1) {
                Toast.makeText(getActivity(), "Events gelöscht", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Event gelöscht", Toast.LENGTH_SHORT).show();
            }
            MainTabHolder.hideUpdateSpinner();
        }
    }

    class ConfirmDeleteDialog extends android.app.DialogFragment {
        ArrayList<Event> mEvents;

        ConfirmDeleteDialog(Context context, final ArrayList<Event> events) {
            mEvents = events;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int eventsCount = mEvents.size();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Löschen?");
            String messageEnd = (eventsCount > 1) ? " Events löschen?" : " Event löschen";
            builder.setMessage("Moechten Sie " + eventsCount + messageEnd);
            builder.setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new DeleteEventsTask().execute(mEvents);
                }
            });
            builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
            return builder.create();
        }
    }

    public void disableActionMode() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    /**
     * adds a event to the view
     *
     * @param event event which should be added
     */
    public void addEventToList(Event event) {
        ((EventsAdapter) mListView.getAdapter()).addItem(event);
    }

    /**
     * just refresh the listview
     */
    public void refreshList() {
        ((EventsAdapter) mListView.getAdapter()).notifyDataSetChanged();
    }

    class EventCABListener implements AbsListView.MultiChoiceModeListener {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            // save count of selected items
            if (checked) {
                mSelectedCount++;
            } else {
                mSelectedCount--;
            }
            // update cab
            mode.invalidate();
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.cab_event, menu);
            MainTabHolder.setPageChangeable(false); // disable swipe to keep user on screen while cab is active

            // customize actionbar
            // hide edit when no rights given
            if (!mModel.isUserManager()) {
                menu.findItem(R.id.cab_event_edit).setVisible(false);
                menu.findItem(R.id.cab_event_delete).setVisible(false);
                // users have lesser options therefore show them directly in actionbar
                menu.findItem(R.id.cab_event_create_booking).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                menu.findItem(R.id.cab_event_show_bookings).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // show or hide items when more than one item is selected
            if (mModel.isUserManager()) {
                if (mSelectedCount > 1) {
                    menu.findItem(R.id.cab_event_edit).setVisible(false);
                } else {
                    menu.findItem(R.id.cab_event_edit).setVisible(true);
                }
            }

            if (mSelectedCount > 1) {
                menu.findItem(R.id.cab_event_create_booking).setVisible(false);
                menu.findItem(R.id.cab_event_share_calendar).setVisible(false);
            } else {
                menu.findItem(R.id.cab_event_create_booking).setVisible(true);
                menu.findItem(R.id.cab_event_share_calendar).setVisible(true);
            }
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.cab_event_edit:
                    editCheckedEvent();
                    mode.finish();
                    return true;
                case R.id.cab_event_delete:
                    deleteCheckedEvents();
                    mode.finish();
                    return true;
                case R.id.cab_event_share_calendar:
                    createCalendarEvent();
                    mode.finish();
                    return true;
                case R.id.cab_event_create_booking:
                    createBookingForEvent();
                    mode.finish();
                    return true;
                case R.id.cab_event_show_bookings:
                    showBookingsSelectedEvents();
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            MainTabHolder.setPageChangeable(true);
            mSelectedCount = 0;
        }
    }
}