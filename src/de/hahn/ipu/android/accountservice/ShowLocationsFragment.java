package de.hahn.ipu.android.accountservice;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.util.ParallelExecutor;
import de.hhn.seb.ipu.app.eventmgmt.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by post_000 on 04.01.14.
 */
public class ShowLocationsFragment extends Fragment implements DisableActionMode {
    private static final String TAG = ShowLocationsFragment.class.getName();
    private LinearLayout mProgressLayout;
    private ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_show_locations, container, false);

        // initilize view
        mProgressLayout = (LinearLayout) view.findViewById(R.id.progress_progress);
        mListView = (ListView) view.findViewById(R.id.show_locations_listview_locations);
        populateView();
        return view;
    }

    public void populateView() {
        mListView.setAdapter(new LocationsAdapter(getActivity()));

        AsyncTask<Void, Void, List<Location>> getLocationsTask = new AsyncTask<Void, Void, List<Location>>() {

            @Override
            protected void onPreExecute() {
                mProgressLayout.setVisibility(View.VISIBLE);
                Log.d(TAG, "in pre");
            }

            @Override
            protected List<Location> doInBackground(Void... params) {
                Log.d(TAG, "in background before");
                List<Location> loc = Model.getReference().getLocations(true);
                Log.d(TAG, "in background after");
                return loc;
            }

            @Override
            protected void onPostExecute(List<Location> locations) {
                ((LocationsAdapter) mListView.getAdapter()).addAll(locations);
                mProgressLayout.setVisibility(View.GONE);
                Log.d(TAG, "in onpost");
            }
        };
        Log.d(TAG, "getLocationsTask execute by: " + Thread.currentThread().getName());
        getLocationsTask.executeOnExecutor(ParallelExecutor.getReference(), (Void) null);
    }

    @Override
    public void disableActionMode() {
        // is implemented because same adapter is used for all fragments
        // does nothing because has no CAB
    }

    /**
     * customize presentation
     */
    public class LocationsAdapter extends BaseAdapter {
        List<Location> mLocations = new ArrayList<>();
        LayoutInflater mInflater;

        public LocationsAdapter(Context context, List location) {
            mLocations = location;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public LocationsAdapter(Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void addAll(List location) {
            mLocations.addAll(location);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mLocations.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            view = mInflater.inflate(R.layout.listitem_fragment_show_locations, null);

            Location location = mLocations.get(position);
            Log.i(TAG, "creating mListView for " + location.getTitle());

            TextView title = (TextView) view.findViewById(R.id.listitem_fragment_show_locations_title);
            if (location.getTitle() != null) title.setText(location.getTitle());

            TextView description = (TextView) view.findViewById(R.id.listitem_fragment_show_locations_description);
            if (location.getDescription() != null) description.setText(location.getDescription());

            TextView url = (TextView) view.findViewById(R.id.listitem_fragment_show_locations_url);
            if (location.getUrl() != null) {
                url.setText(location.getUrl().toString());
            } else {
                url.setText("Keine URL vorhanden");
            }

            return view;
        }
    }
}