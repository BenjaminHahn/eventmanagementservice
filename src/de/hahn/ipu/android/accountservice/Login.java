package de.hahn.ipu.android.accountservice;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.dialog.MessageDialog;
import de.hahn.ipu.android.accountservice.util.ClearErrorWatcher;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;

/**
 * Created by post_000 on 03.01.14.
 */
public class Login extends Activity {
    private static final String TAG = Login.class.getName();
    private final Login thisActivity = this;
    EditText mEdtUsername;
    EditText mEdtPassword;
    String mUsername;
    String mPassword;
    ProgressBar mProgress;
    private Model mModel = Model.getReference();
    private static boolean mLoggingIn;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        mEdtUsername = (EditText) findViewById(R.id.login_edt_username);
        mEdtPassword = (EditText) findViewById(R.id.login_edt_password);

        Intent intent = getIntent();
        String username = intent.getStringExtra("username");
        if (username != null) mEdtUsername.setText(username);
        String password = intent.getStringExtra("password");
        if (password != null) mEdtPassword.setText(password);

        mProgress = (ProgressBar) findViewById(R.id.login_progress_spinner);
        findViewById(R.id.login_btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performLogin();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.register:
                startActivity(new Intent(this, EditUser.class));
                break;
        }
        return true;
    }

    /**
     * perform the login into the service via username and password obtained from the textfields
     */
    public void performLogin() {
        View focus = null;
        boolean cancel = false;

        // test on fields empty
        mUsername = mEdtUsername.getText().toString();
        mEdtUsername.addTextChangedListener(new ClearErrorWatcher(mEdtUsername));
        if (mUsername.isEmpty()) {
            mEdtUsername.setError("Bitte Nutzernamen eingeben");
            focus = mEdtUsername;
            cancel = true;
        }

        mPassword = mEdtPassword.getText().toString();
        mEdtPassword.addTextChangedListener(new ClearErrorWatcher(mEdtPassword));
        if (mPassword.isEmpty()) {
            mEdtPassword.setError("Bitte Passwort eingeben");
            focus = mEdtPassword;
            cancel = true;
        }

        if (cancel) {
            focus.requestFocus();
        } else {

            AsyncTask<Void, Void, Boolean> loginTask = new AsyncTask<Void, Void, Boolean>() {

                @Override
                protected void onPreExecute() {
                    mProgress.setVisibility(ProgressBar.VISIBLE);
                }

                @Override
                protected Boolean doInBackground(Void... params) {
                    // try login and open up a message dialog on wrong parameters
                    if (mModel.login(mUsername, mPassword)) {
                        return null;
                    } else {
                        // show dialog to inform user that username or password ist wrong or not existing
                        new MessageDialog("Fehler", "Nutzername oder Passwort falsch!").show(getFragmentManager(), "InvalidParameter");
                        this.cancel(true);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mLoggingIn = false;
                                mProgress.setVisibility(ProgressBar.GONE);
                            }
                        });
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Boolean bool) {
                    if (!isCancelled()) {
                        Log.i(TAG, "Login successful");
                        Model model = Model.getReference();
                        User user = model.getUser();
                        startActivity(new Intent(thisActivity, MainTabHolder.class));
                        Toast.makeText(thisActivity, "Willkommen, " + user.getFirstname() + " " + user.getLastname(), Toast.LENGTH_LONG).show();
                        mLoggingIn = false;
                    }
                    mProgress.setVisibility(ProgressBar.GONE);
                }
            };
            // check if login attemmpt already running
            if (!mLoggingIn) {
                mLoggingIn = true;
                loginTask.execute((Void) null);
            }
        }
    }
}