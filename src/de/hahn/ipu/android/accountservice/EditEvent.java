package de.hahn.ipu.android.accountservice;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.adapter.SingleLineLocationsAdapter;
import de.hahn.ipu.android.accountservice.datetimepicker.DatePickerFragment;
import de.hahn.ipu.android.accountservice.datetimepicker.TimePickerFragment;
import de.hahn.ipu.android.accountservice.dialog.MessageDialog;
import de.hahn.ipu.android.accountservice.util.ClearErrorWatcher;
import de.hahn.ipu.android.accountservice.util.Helpers;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.Location;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by post_000 on 04.01.14.
 */
public class EditEvent extends FragmentActivity {
    private static final String TAG = EditEvent.class.getName();
    private Button mStartDate;
    private Button mStartTime;
    private Button mEndDate;
    private Button mEndTime;
    private Spinner mSpinLocations;
    private boolean mEditExisting;
    private EditText mTitleView;
    private EditText mDescriptionView;
    private TextView mParticipantsView;
    private CheckBox mAllDayEvent;
    private CheckBox mIndefiniteParticipants;
    private Event mEventToEdit;
    private Event mNewEvent;
    private boolean mCancel;
    private EditEvent mThisActivity = this;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done_cancel, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                performAddEvent();
                break;
            case R.id.cancel:
                Toast.makeText(this, "Erstellung abgebrochen", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        setTitle("Event erstellen");
        getActionBar().setDisplayHomeAsUpEnabled(true);


        // check if event exist
        // when existing edit it else create new
        mEventToEdit = Helpers.getEventFromHolder();
        mEditExisting = mEventToEdit != null;

        // initialize views
        Calendar c = Calendar.getInstance();
        mStartDate = ((Button) findViewById(R.id.start_date));
        mStartDate.setText(Helpers.formatDateToStringDate(c.getTime()));
        mStartTime = ((Button) findViewById(R.id.start_time));
        mStartTime.setText(Helpers.formatDateToStringTime(c.getTime()));
        mEndDate = ((Button) findViewById(R.id.end_date));
        mEndDate.setText(Helpers.formatDateToStringDate(c.getTime()));
        mEndTime = ((Button) findViewById(R.id.end_time));
        mEndTime.setText(Helpers.formatDateToStringTime(c.getTime()));

        mTitleView = ((EditText) findViewById(R.id.edit_event_text_title));
        mDescriptionView = ((EditText) findViewById(R.id.edit_event_text_description));
        mParticipantsView = (TextView) findViewById(R.id.edit_event_participants);
        mAllDayEvent = (CheckBox) findViewById(R.id.edit_event_allday);
        mAllDayEvent.setOnCheckedChangeListener(new AllDayCheckedListener());
        mIndefiniteParticipants = (CheckBox) findViewById(R.id.edit_event_participants_indefinite);
        mIndefiniteParticipants.setOnCheckedChangeListener(new IndefiniteParticipantsChangeListener());

        mSpinLocations = (Spinner) findViewById(R.id.create_event_spinner_locations);
        List<Event> locations = new ArrayList(Model.getReference().getLocations(false));
        mSpinLocations.setAdapter(new SingleLineLocationsAdapter(this, locations, true));

        // set views when editing event
        if (mEditExisting) {
            setForm();
        }
    }

    /**
     * set data from event into form when editing existing event
     */
    private void setForm() {
        mTitleView.setText(mEventToEdit.getTitle());
        mDescriptionView.setText(mEventToEdit.getDescription());

        // check indefinite participants and set checkbox or set textview
        if (mEventToEdit.getMaxNoParticipants() == Integer.MAX_VALUE) {
            mIndefiniteParticipants.setChecked(true);
        } else {
            mParticipantsView.setText(mEventToEdit.getMaxNoParticipants() + "");
        }
        mAllDayEvent.setChecked(mEventToEdit.isAllDayEvent());

        mStartDate.setText(Helpers.formatDateToStringDate(mEventToEdit.getBeginsAt()));
        mStartTime.setText(Helpers.formatDateToStringTime(mEventToEdit.getBeginsAt()));
        mEndDate.setText(Helpers.formatDateToStringDate(mEventToEdit.getEndsAt()));
        mEndTime.setText(Helpers.formatDateToStringTime(mEventToEdit.getEndsAt()));

        SingleLineLocationsAdapter adapter = (SingleLineLocationsAdapter) mSpinLocations.getAdapter();
        mSpinLocations.setSelection(adapter.getIndexOfLocation(mEventToEdit.getLocation()));
    }

    /**
     * get data from form and save into event
     */
    private Event saveData() {
        mCancel = false;

        // check empty fields and wrong input
        String title = mTitleView.getText().toString();
        mTitleView.addTextChangedListener(new ClearErrorWatcher(mTitleView));
        if (title.isEmpty()) {
            mTitleView.setError("Feld wird benötigt");
            mCancel = true;
            return null;    // stop execution premature
        }

        // description
        String description = mDescriptionView.getText().toString();
        mDescriptionView.addTextChangedListener(new ClearErrorWatcher(mDescriptionView));
        if (description.isEmpty()) {
            mDescriptionView.setError("Feld wird benötigt");
            mCancel = true;
            return null;
        }

        // participants check empty field
        String participants = mParticipantsView.getText().toString();
        mParticipantsView.addTextChangedListener(new ClearErrorWatcher(mParticipantsView));
        if (participants.isEmpty() && mParticipantsView.isEnabled()) {
            mParticipantsView.setError("Feld wird benötigt");
            mCancel = true;
            return null;
        }

        int noParticipants = 0;
        if (mIndefiniteParticipants.isChecked()) {
            noParticipants = Integer.MAX_VALUE;
        } else {
            // participants check valid input
            try {
                noParticipants = Integer.parseInt(participants);
            } catch (NumberFormatException e) {
                mParticipantsView.setError("Bitte eine Ganzzahl angeben");
                mCancel = true;
                return null;
            }
        }


        if (!mCancel) {
            // location
            Location location = (Location) mSpinLocations.getSelectedItem();
            boolean allDayEvent = mAllDayEvent.isChecked();

            // parse string date and time from view put it into date objects
            Calendar date = Calendar.getInstance();
            Calendar time = Calendar.getInstance();
            Calendar both = Calendar.getInstance();

            Date date0 = Helpers.parseStringToDateDate(mStartDate.getText().toString());
            Date time0 = Helpers.parseStringToDateTime(mStartTime.getText().toString());
            date.setTime(date0);
            time.setTime(time0);
            both.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE),
                    time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
            Date beginsAt = both.getTime();

            Date date1 = Helpers.parseStringToDateDate(mEndDate.getText().toString());
            Date time1 = Helpers.parseStringToDateTime(mEndTime.getText().toString());
            date.setTime(date1);
            time.setTime(time1);
            both.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE),
                    time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
            Date endsAt = both.getTime();

            // check if begin and date makes sense
            if (beginsAt.after(endsAt)) {
                mCancel = true;
                Toast.makeText(this, "Das Beginndatum muss vor dem Enddatum liegen", Toast.LENGTH_SHORT).show();
            }


            // set data into existing event or create new one
            if (mEditExisting) {
                mNewEvent = null;
                // check if max paticipants event larger actual participants
                if (noParticipants < mEventToEdit.getNoActualParticipants()) {
                    mCancel = true;
                    new MessageDialog("Fehler", "AktuelleTeilnehmerzahl(" + mEventToEdit.getNoActualParticipants() +
                            ") überschreitet die maximale Teilnehmerzahl(" + noParticipants +
                            ")/n Bitte zuerst Buchungen löschen").show(getFragmentManager(), "EditEvent");
                } else {
                    // everything fine set event
                    mEventToEdit.setTitle(title);
                    mEventToEdit.setDescription(description);
                    mEventToEdit.setLocation(location);
                    mEventToEdit.setBeginsAt(beginsAt);
                    mEventToEdit.setEndsAt(endsAt);
                    mEventToEdit.setMaxNoParticipants(noParticipants);
                    mEventToEdit.setAllDayEvent(allDayEvent);
                }
            } else {
                // create new one
                mEventToEdit = null;
                mNewEvent = new Event(title, description, location, beginsAt, endsAt);
                mNewEvent.setReservationPossible(true);
                mNewEvent.setMaxNoParticipants(noParticipants);
                mNewEvent.setAllDayEvent(allDayEvent);
            }
        }
        if (mCancel) {
            return null;
        } else {
            return mEditExisting ? mEventToEdit : mNewEvent;
        }
    }

    /**
     * show timepicker
     *
     * @param v the calling view which gets set
     */
    public void showTimePickerDialog(View v) {
        Button viewToSet = (Button) v;
        DialogFragment newFragment = new TimePickerFragment(viewToSet);
        newFragment.show(getFragmentManager(), "timePicker");
    }

    /**
     * show datepicker
     *
     * @param v the calling view which gets set
     */
    public void showDatePickerDialog(View v) {
        Button viewToSet = (Button) v;
        DialogFragment newFragment = new DatePickerFragment(viewToSet);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    /**
     * get the data, create and add or edit a event
     */
    public void performAddEvent() {
        Event event = saveData();
        if (event != null) {
            new AddEventTask().execute(event);
            finish();
        }
    }

    /**
     * return result what has been done
     * return a event when new one was created
     */
    @Override
    public void finish() {
        if (mEditExisting) {
            setResult(MainTabHolder.EDIT_EVENT);
        } else {
            Intent intent = new Intent();
            intent.putExtra("event", mNewEvent);
            System.out.println("new Event " + mNewEvent);
            setResult(MainTabHolder.CREATE_EVENT, intent);
        }
        super.finish();
    }

    class AddEventTask extends AsyncTask<Event, Void, Void> {

        @Override
        protected void onPreExecute() {
            MainTabHolder.showUpdatespinner();
        }

        @Override
        protected Void doInBackground(Event... event) {
            if (Model.getReference().addEvent(event[0])) {
                return null;
            } else {
                this.cancel(true);
                return null;
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(mThisActivity, "Event konnte nicht erstellt werden", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (mEditExisting) {
                Toast.makeText(mThisActivity, "Event wurde verändert", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mThisActivity, "Event wurde hinzugefügt", Toast.LENGTH_SHORT).show();
            }
            MainTabHolder.hideUpdateSpinner();
        }
    }

    class AllDayCheckedListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                mStartTime.setVisibility(View.GONE);
                mEndTime.setVisibility(View.GONE);
            } else {
                mStartTime.setVisibility(View.VISIBLE);
                mEndTime.setVisibility(View.VISIBLE);
            }
        }
    }

    class IndefiniteParticipantsChangeListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mParticipantsView.setEnabled(!isChecked);
            if (isChecked) {
                mParticipantsView.setHint("unbegrenzt");
            } else {
                mParticipantsView.setHint("max. Teilnehmer");
            }
        }
    }
}