package de.hahn.ipu.android.accountservice;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import de.hahn.ipu.android.accountservice.adapter.SingleLineEventsAdapter;
import de.hahn.ipu.android.accountservice.adapter.SingleLineLocationsAdapter;
import de.hahn.ipu.android.accountservice.adapter.SingleLineUsersAdapter;
import de.hahn.ipu.android.accountservice.dialog.BookingDetailsDialog;
import de.hahn.ipu.android.accountservice.util.Helpers;
import de.hahn.ipu.android.accountservice.util.ParallelExecutor;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.Location;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by post_000 on 03.01.14.
 */
public class ShowBookingsFragment extends Fragment implements DisableActionMode {
    private static final String TAG = ShowBookingsFragment.class.getName();
    private static final String EVENT_FILTER = "Event";
    private static final String LOCATION_FILTER = "Location";
    private static final String USER_FILTER = "Nutzer";
    public static final int CREATE_BOOKING = 1;
    public static final int EDIT_BOOKING = 2;
    boolean mListLoading = false;
    private Spinner mEventsSpinner = null;
    private Spinner mLocationsSpinner = null;
    private Spinner mUsersSpinner = null;
    private Model mModel;
    private ListView mListView;
    private BookingsAdapter mListViewAdapter;
    private int mSelectedCount;
    private MainTabHolder mHolder;
    private LinearLayout mProgressLayout;
    private ActionMode mActionMode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_show_bookings, container, false);
        mModel = Model.getReference();
        mHolder = ((MainTabHolder) getActivity());

        // configurate listview
        mProgressLayout = (LinearLayout) view.findViewById(R.id.progress_progress);
        mListView = (ListView) view.findViewById(R.id.show_bookings_listview_bookings);
        mListView.setEmptyView(view.findViewById(R.id.listitem_empty_item));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Booking booking = (Booking) parent.getAdapter().getItem(position);
                BookingDetailsDialog dialog = new BookingDetailsDialog(booking);
                dialog.show(mHolder.getSupportFragmentManager(), "Details");
            }
        });
        mListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new BookingCABListener());

        mEventsSpinner = (Spinner) view.findViewById(R.id.show_bookings_spinner_events);
        mEventsSpinner.setOnItemSelectedListener(new FilterListListener());

        mLocationsSpinner = (Spinner) view.findViewById(R.id.show_bookings_spinner_locations);
        mLocationsSpinner.setOnItemSelectedListener(new FilterListListener());

        mUsersSpinner = (Spinner) view.findViewById(R.id.show_bookings_spinner_users);
        mUsersSpinner.setOnItemSelectedListener(new FilterListListener());

        // when user is not rolemanager he always sees just his own bookings. there's no need for this filter
        if (!mModel.isUserManager()) {
            mUsersSpinner.setVisibility(Spinner.GONE);
        }

        populateView();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        switch (requestCode) {
            case CREATE_BOOKING:
                populateView();
                break;
            case EDIT_BOOKING:
                refreshList();
                break;
        }
    }

    /**
     * populate the bookingsfragment with data from service
     */
    public void populateView() {
        ParallelExecutor executor = ParallelExecutor.getReference();
        Log.d(TAG, "populateView");
        new GetEventsTask().executeOnExecutor(executor, (Void) null);
        new GetLocationsTask().executeOnExecutor(executor, (Void) null);
        new GetUsersTask().executeOnExecutor(executor, (Void) null);
        new GetBookingsTask().executeOnExecutor(executor, (Void) null);
    }

    private void editCheckedBooking() {
        Log.d(TAG, "editCheckedBooking");
        Adapter adapter = mListView.getAdapter();
        int[] checkedPositions = getCheckedPositions();
        Booking booking = (Booking) adapter.getItem(checkedPositions[0]);
        Helpers.putBookingIntoHolder(booking);
        EditBookingDialog dialog = new EditBookingDialog();
        dialog.show(getActivity().getSupportFragmentManager(), "editBooking");
        dialog.setTargetFragment(this, EDIT_BOOKING);
    }

    /**
     * create a event in google calender for selected event
     */
    private void createCalendarEvent() {
        Log.d(TAG, "createCalendarEvent");
        Adapter adapter = mListView.getAdapter();
        int[] checkedPositions = getCheckedPositions();
        Booking booking = (Booking) adapter.getItem(checkedPositions[0]);
        Event event = mModel.getEventViaId(booking.getEventId());

        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("title", event.getBeginsAt());
        intent.putExtra("allDay", event.isAllDayEvent());
        intent.putExtra("endTime", event.getEndsAt());
        intent.putExtra("title", event.getTitle());
        intent.putExtra("eventLocation", event.getLocation().getTitle());
        intent.putExtra("description", event.getDescription());
        startActivity(intent);
    }

    private void deleteCheckedBookings() {
        Log.d(TAG, "deleteCheckedBookings");
        Adapter adapter = mListView.getAdapter();
        ArrayList<Booking> bookings = new ArrayList<>();
        int[] checkedPositions = getCheckedPositions();
        for (int i : checkedPositions) {
            bookings.add((Booking) adapter.getItem(i));
        }
        new ConfirmDeleteDialog(getActivity(), bookings).show(getActivity().getFragmentManager(), "confirmDialog");
    }

    /**
     * get positions of selected items
     *
     * @return item position
     */
    private int[] getCheckedPositions() {
        Log.d(TAG, "getCheckedPositions");
        ArrayList<Integer> positions = new ArrayList<>();
        Adapter adapter = mListView.getAdapter();
        SparseBooleanArray checkedItems = mListView.getCheckedItemPositions();
        for (int i = 0; i < adapter.getCount(); i++) {
            if (checkedItems.get(i)) {
                positions.add(i);
            }
        }
        // convert to primitive
        int[] pos = new int[positions.size()];
        for (int j = 0; j < pos.length; j++) {
            pos[j] = positions.get(j);
        }
        return pos;
    }

    /**
     * filter the shown list accourding to the filters representing by spinners
     */
    private void filterList() {
        // prevent filtering while list is updated
        if (!mListLoading) {
            Log.d(TAG, "filterList");
            new FilterTask().execute((Void) null);
        }
    }

    public void setListViewItems(List<Booking> items) {
        Log.d(TAG, "setListViewItems");

        setSelectionProgramatically(mEventsSpinner, 0);
        setSelectionProgramatically(mLocationsSpinner, 0);
        setSelectionProgramatically(mUsersSpinner, 0);

        mListViewAdapter.setItems(items);
        Log.d(TAG, "set external booking list");
    }

    /**
     * substitude listitem with progress spinner
     *
     * @param bool show or hode progress
     */
    public void showProgressLayout(boolean bool) {
        if (bool) {
            mProgressLayout.setVisibility(LinearLayout.VISIBLE);
        } else {
            mProgressLayout.setVisibility(LinearLayout.GONE);
        }
    }

    public void disableActionMode() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    /**
     * every time a spinner is updated programmatically the call of the listener should be ignored
     *
     * @param spinner the spinner which should be set
     * @param index   the index of the new position
     */
    public void setSelectionProgramatically(Spinner spinner, int index) {
        spinner.setSelection(index);
        ((FilterListListener) spinner.getOnItemSelectedListener()).ignoreNext();
    }

    /**
     * just refresh the listview
     */
    public void refreshList() {
        ((BookingsAdapter) mListView.getAdapter()).notifyDataSetChanged();
    }

    /**
     * customize presentation
     */
    class BookingsAdapter extends BaseAdapter {
        List<Booking> mBookings;
        LayoutInflater mInflater;

        public BookingsAdapter(Context context, List<Booking> bookings) {
            mBookings = bookings;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /**
         * add item to list without sync to service
         *
         * @param booking which should be added
         */
        public void addItem(Booking booking) {
            mBookings.add(booking);
        }

        /**
         * replace all items
         *
         * @param items which should be shown
         */
        public void setItems(List<Booking> items) {
            mBookings = items;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }

        /**
         * delete all items without sync to service
         *
         * @param bookings which should be deleted
         */
        public void deleteItems(List<Booking> bookings) {
            for (Booking booking : bookings) {
                mBookings.remove(booking);
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getCount() {
            return mBookings.size();
        }

        @Override
        public Object getItem(int position) {
            return mBookings.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // inflate different layout on different roles
            View view = mInflater.inflate(R.layout.listitem_fragment_show_bookings, null);

            Booking booking = mBookings.get(position);
            Event event = mModel.getEventViaId(booking.getEventId());

            Log.d(TAG, "creating listview for " + booking.getId());

            TextView title = (TextView) view.findViewById(R.id.show_bookings_title);
            title.setText(booking.getId() + " - " + event.getTitle());

            TextView location = (TextView) view.findViewById(R.id.show_bookings_where);
            location.setText(event.getLocation().getTitle());

            Calendar calendar = Calendar.getInstance();

            TextView beginsAtView = (TextView) view.findViewById(R.id.show_bookings_begin);
            Date beginsAt = event.getBeginsAt();
            calendar.setTime(beginsAt);
            beginsAtView.setText(Helpers.getDayName(calendar) + ", " + Helpers.formatDateToStringDate(beginsAt) + " " + Helpers.formatDateToStringTime(beginsAt));

            TextView endsAtView = (TextView) view.findViewById(R.id.show_bookings_end);
            Date endsAt = event.getEndsAt();
            calendar.setTime(beginsAt);
            endsAtView.setText(Helpers.getDayName(calendar) + ", " + Helpers.formatDateToStringDate(endsAt) + " " + Helpers.formatDateToStringTime(endsAt));

            TextView participant = (TextView) view.findViewById((R.id.show_bookings_participants));
            participant.setText(booking.getNoParticipants() + "");

            // other layout for manager
            if (mModel.isUserManager()) {
                TextView username = (TextView) view.findViewById((R.id.show_bookings_user_reserved));
                username.setText(booking.getUserName());
            } else {
                view.findViewById(R.id.show_bookings_row_reserved).setVisibility(View.GONE);
            }
            return view;
        }
    }

    class FilterTask extends AsyncTask<Void, Void, ArrayList<Booking>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressLayout(true);
        }

        @Override
        protected ArrayList<Booking> doInBackground(Void... params) {
            Event eventFilter = (Event) mEventsSpinner.getSelectedItem();
            Location locationFilter = (Location) mLocationsSpinner.getSelectedItem();
            User userFilter = (User) mUsersSpinner.getSelectedItem();
            ArrayList<Booking> filteredList = new ArrayList<>();
            List<Booking> list = new ArrayList(mModel.getAllBookings(false));

            for (Booking booking : list) {
                String locationTitle = mModel.getEventViaId(booking.getEventId()).getLocation().getTitle();
                if (booking.getEventId().equals(eventFilter.getId()) || eventFilter.getTitle().equals(EVENT_FILTER)) {
                    if (locationTitle.equals(locationFilter.getTitle()) || locationFilter.getTitle().equals(LOCATION_FILTER)) {
                        if (booking.getUserName().equals(userFilter.getUsername()) || userFilter.getUsername().equals(USER_FILTER)) {
                            filteredList.add(booking);
                        }
                    }
                }
            }
            return filteredList;
        }

        @Override
        protected void onPostExecute(ArrayList<Booking> filteredList) {
            Log.d(TAG, "replace with filteredList");
            mListViewAdapter.setItems(filteredList);
            showProgressLayout(false);
        }
    }

    class GetBookingsTask extends AsyncTask<Void, Void, ArrayList<Booking>> {
        @Override
        protected void onPreExecute() {
            showProgressLayout(true);
            mListLoading = true;
            mHolder.showUpdatespinner();
            Log.d(TAG, "in pre");
        }

        @Override
        protected ArrayList<Booking> doInBackground(Void... params) {
            return mModel.getAllBookings(true);
        }

        @Override
        protected void onPostExecute(ArrayList<Booking> bookings) {
            Log.i(TAG, "populate listview now");
            mListViewAdapter = new BookingsAdapter(getActivity(), bookings);
            mListView.setAdapter(mListViewAdapter);
            mHolder.hideUpdateSpinner();
            mProgressLayout.setVisibility(View.GONE);
            showProgressLayout(false);
            mListLoading = false;
        }
    }

    class GetUsersTask extends AsyncTask<Void, Void, ArrayList<User>> {

        @Override
        protected ArrayList<User> doInBackground(Void... params) {
            ArrayList<User> users = new ArrayList(mModel.getUsers());
            users.add(0, new User(USER_FILTER, null, null, null, null));
            return users;
        }

        @Override
        protected void onPostExecute(ArrayList<User> users) {
            mUsersSpinner.setAdapter(new SingleLineUsersAdapter(getActivity(), users));
        }
    }

    class GetLocationsTask extends AsyncTask<Void, Void, ArrayList<Location>> {

        @Override
        protected ArrayList<Location> doInBackground(Void... params) {
            Location defaultLocation = new Location(LOCATION_FILTER, null, null);
            ArrayList<Location> locations = new ArrayList(mModel.getLocations(true));
            locations.add(0, defaultLocation);
            return locations;
        }

        @Override
        protected void onPostExecute(ArrayList<Location> locations) {
            mLocationsSpinner.setAdapter(new SingleLineLocationsAdapter(getActivity(), locations));
        }
    }

    class GetEventsTask extends AsyncTask<Void, Void, ArrayList<Event>> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected ArrayList<Event> doInBackground(Void... params) {
            Event defaultEvent = new Event(EVENT_FILTER, null, null, null, null);
            ArrayList<Event> events = new ArrayList<>(mModel.getEvents(true));
            events.add(0, defaultEvent);
            return events;
        }

        @Override
        protected void onPostExecute(ArrayList<Event> events) {
            mEventsSpinner.setAdapter(new SingleLineEventsAdapter(getActivity(), events));
        }
    }

    class DeleteBookingsTask extends AsyncTask<ArrayList<Booking>, Void, Void> {
        int bookingsCount;

        @Override
        protected Void doInBackground(ArrayList<Booking>... bookings) {
            bookingsCount = bookings[0].size();
            ((BookingsAdapter) mListView.getAdapter()).deleteItems(bookings[0]);
            mModel.deleteBookings(bookings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (bookingsCount > 1) {
                Toast.makeText(getActivity(), "Buchungen gelöscht", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Buchung gelöscht", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class ConfirmDeleteDialog extends android.app.DialogFragment {
        ArrayList<Booking> mBookings;

        ConfirmDeleteDialog(Context context, final ArrayList<Booking> bookings) {
            mBookings = bookings;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int bookingsCount = mBookings.size();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Löschen?");
            String messageEnd = (bookingsCount > 1) ? " Buchungen löschen?" : " Buchung löschen";
            builder.setMessage("Moechten Sie " + bookingsCount + messageEnd);
            builder.setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new DeleteBookingsTask().execute(mBookings);
                }

            });
            builder.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
            return builder.create();
        }
    }

    class FilterListListener implements AdapterView.OnItemSelectedListener {
        boolean mIgnore;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (mIgnore) {
                mIgnore = false;
            } else {
                filterList();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

        /**
         * ignore the next call of the listener
         */
        public void ignoreNext() {
            mIgnore = true;
        }
    }

    class BookingCABListener implements AbsListView.MultiChoiceModeListener {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            // save count of selected items
            if (checked) {
                mSelectedCount++;
            } else {
                mSelectedCount--;
            }
            // update cab
            mode.invalidate();
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater infalter = getActivity().getMenuInflater();
            infalter.inflate(R.menu.cab_booking, menu);
            mActionMode = mode;
            MainTabHolder.setPageChangeable(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            if (mSelectedCount > 1) {
                menu.findItem(R.id.cab_booking_share_calendar).setVisible(false);
                menu.findItem(R.id.cab_booking_edit).setVisible(false);
            } else {
                menu.findItem(R.id.cab_booking_share_calendar).setVisible(true);
                menu.findItem(R.id.cab_booking_edit).setVisible(true);
            }
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.cab_booking_edit:
                    editCheckedBooking();
                    mode.finish();
                    return true;
                case R.id.cab_booking_share_calendar:
                    createCalendarEvent();
                    mode.finish();
                    return true;
                case R.id.cab_booking_delete:
                    deleteCheckedBookings();
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            MainTabHolder.setPageChangeable(true);
            mSelectedCount = 0;
        }
    }
}