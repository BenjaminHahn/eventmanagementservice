package de.hahn.ipu.android.accountservice.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.R;
import de.hhn.seb.ipu.app.eventmgmt.Event;

/**
 * Created by post_000 on 07.01.14.
 */
public class EventDetailsDialog extends DialogFragment {
    private Event mEvent;

    public EventDetailsDialog() {
    }

    public EventDetailsDialog(Event event) {
        mEvent = event;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_listitem_details, null);

        TextView title = (TextView) view.findViewById(R.id.dialog_listitem_details_title);
        title.setText(mEvent.getTitle() + " - " + mEvent.getLocation().getTitle());

        TextView description = (TextView) view.findViewById(R.id.dialog_listitem_details_description);
        description.setText(mEvent.getDescription());

        // hide third textview here its only used in booking details as a comment
        ((TextView) view.findViewById(R.id.dialog_listitem_details_comment)).setVisibility(TextView.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        return builder.create();
    }
}