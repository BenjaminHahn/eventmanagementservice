package de.hahn.ipu.android.accountservice.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by post_000 on 10.01.14.
 */
public class MessageDialog extends DialogFragment {
    String mMessage;
    String mTitle;

    public MessageDialog(String title, String message) {
        mTitle = title;
        mMessage = message;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        return builder.create();
    }
}
