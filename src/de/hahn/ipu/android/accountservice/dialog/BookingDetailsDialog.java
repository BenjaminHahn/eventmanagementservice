package de.hahn.ipu.android.accountservice.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import de.hahn.ipu.android.accountservice.Model;
import de.hahn.ipu.android.accountservice.R;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;

/**
 * Created by post_000 on 10.01.14.
 */
public class BookingDetailsDialog extends DialogFragment {
    private Booking mBooking;

    public BookingDetailsDialog() {
    }

    public BookingDetailsDialog(Booking booking) {
        mBooking = booking;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_listitem_details, null);
        Event event = Model.getReference().getEventViaId(mBooking.getEventId());

        TextView title = (TextView) view.findViewById(R.id.dialog_listitem_details_title);
        title.setText(event.getTitle() + " - " + event.getLocation().getTitle());

        TextView description = (TextView) view.findViewById(R.id.dialog_listitem_details_description);
        description.setText(event.getDescription());

        TextView commentView = (TextView) view.findViewById(R.id.dialog_listitem_details_comment);
        String comment = mBooking.getAttributes().get("comment");
        if (comment != null && !comment.isEmpty()) {
            commentView.setText("Kommentar: " + comment);
        } else {
            commentView.setVisibility(View.GONE);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        return builder.create();
    }
}
